# Karten zu ygo dominion html hinzufügen
###################################
# Author: Robin Baumann
#
# Version: 03.00
#
# Datum: 25.02.2022
###################################

# Globals
CATRGORIES = ["monster", "spell", "trap", "extra"]

# Nullen am Anfang entfernen
def remove_zeros(id):
    while id[0] == "0":
        id = id[1:]
    return id


# Kommentare entfernen
def remove_comment(id):
    start_remove = 0
    i = 0
    while id[i] != '\n':
        if id[i] == '\t':
            start_remove = 1
        if id[i] == '\n':
            start_remove = 0
        if start_remove == 1:
            id = id[:i]+id[i+1:]
            i = i-1
        i = i+1
    return id


# html updaten
def update_cards_from_txt(txt_directory, html_directory):

    # Text-File öffnen mit allen Karten IDs
    with open(txt_directory, 'r') as cards:

        # Text auslesen
        Cards = cards.readlines()

        # Leerzeilen Nullen in KartenID und Kommentare entfernen
        Cards = list(filter(lambda id: id != '\n', Cards))
        Cards = list(map(remove_zeros, Cards))
        Cards = list(map(remove_comment, Cards))

        # Umburch entfernen
        Cards = [id[:-1] for id in Cards]

    # HTML-File öffnen und Kartenliste erstellen
    with open(html_directory, 'r') as html:

        # Sting alegen wo am Ende alle Karten drin stehen sollen
        Card_strings = ""

        # html als Liste auslesen und als string zusammenfügen
        html_string = ''.join(html.readlines())

        # Comments finden und Inhalt davor und danach speichern
        begin_cards = html_string.find(
            '<!-- Begin Cards here-->') + len('<!-- Begin Cards here-->')
        html_begin = html_string[:begin_cards]
        begin_end = html_string.find('<!-- Add Card in Format')
        html_end = html_string[begin_end:]

        currentCategory = 0
        cardCount = 0

        # Durch alle Karten loopen
        for i, card in enumerate(Cards):

            # zählen wie oft die Karte bis hier vorkommt
            count = 0
            for card_prev in Cards[0:i]:
                if card_prev == card:
                    count += 1

            # Card ID anpassen
            card_id = f'{card}-{count+1}' 

            # Cardstring erstellen
            if not card.isdigit():
                Card_strings += f'\t\t\t<h2 id="{card}Headder" class="display-5 border-bottom text-white pb-3 mb-5 mt-5">{card}</h2>\n'
                currentCategory += 1

            else:
                cardNumber = "cardNumber" + str(cardCount)
                cardCount += 1
                Card_strings +=     f'\t\t\t<button class="cardButton {CATRGORIES[currentCategory-1]} {cardNumber}" onclick="selectCard(\'{card_id}\')">\n \
                                    \t\t\t\t<img id="{card_id}" src="https://images.ygoprodeck.com/images/cards/{card}.jpg">\n \
                                    \t\t\t</button>\n'
                                                                                                                                 
    # Kartenliste in HTML File schreiben
    with open(html_directory, 'w') as html:

        html_new = html_begin + "\n" + Card_strings + "\n\n\t" + html_end
        html.write(html_new)


# File pathss
# directory = 'C:/Users/robin/OneDrive/Python/ygo-dominion-git/'#
# directory = 'E:\Develop\Code\VS\ygo-dominion/'#
directory = 'C:/Users/Marvi/Documents/VS Code/Git Lab/ygo-dominion/'
filename_main_txt = 'EDISON_DRAFT_DOMINION_main.txt'
filename_main_html = 'ygo_main.html'
# filename_extra_txt = 'EDISON_DRAFT_DOMINION_extra.txt'
# filename_extra_html = 'ygo_extra.html'


# do the update for Main and Extra Deck files
update_cards_from_txt(directory+filename_main_txt,
                      directory+filename_main_html)

# update_cards_from_txt(directory+filename_extra_txt,
#                       directory+filename_extra_html)
