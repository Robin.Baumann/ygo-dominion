    // Define Player Colors
    var player = [["red", 0, "rgb(196, 60, 80)", 0, 0],
    ["green", 0, "rgb(103, 241, 103)", 0, 0],
    ["blue", 0, "rgb(55, 110, 240)", 0, 0],
    ["white", 0, "white", 0, 0],
    ["yellow", 0, "rgb(255, 255, 108)", 0, 0],
    ["pink", 0, "rgb(252, 100, 231)", 0, 0],
    ["orang", 0, "rgb(255, 104, 66)", 0, 0]];

    var isInModal = false;

    const delay = ms => new Promise(res => setTimeout(res, ms));

    document.addEventListener('click', function(event) {
        var isClickInsideSideBar = document.getElementById("left-controls").contains(event.target);
        isInModal = document.getElementById("modals").contains(event.target);
        if (!isClickInsideSideBar && !isInModal) {
            closeMenu();
        }
        isInModal = false;
    });

    async function selectCard(card) {

        var cardNode = document.getElementById(card);
        var isAlreadySelected = false;
        if(cardNode.parentNode.classList.contains("selectedCard")){
            isAlreadySelected = true;	
        }

        // Schauen ob sideBar offen ist
        if (document.getElementById("left-controls").classList.contains("expandend") && !isAlreadySelected) {

            closeMenu();

        } else{

            // aktueller Spieler
            var turn_player_color = "";

            // aktuellen Player finden
            var currentPlayer;
            for (var i = 0; i < player.length; i++) {
                if (player[i][1]){
                    turn_player_color = player[i][0];
                    currentPlayer = player[i];
                } 
            }

            var isExtra = cardNode.parentNode.classList.contains("extra");

            if(turn_player_color == ""){

                alert("Wähl Farbe du Hurensohn");
            }
            else if(isAlreadySelected == true) {

                cardNode.parentNode.classList.add("picking");
                await delay(100);
                cardNode.parentNode.classList.remove("picking");

                cardNode.parentNode.classList.remove("selectedCard");
                var placeholderDiv = document.getElementsByClassName(card)[0];
                insertAfter(placeholderDiv, cardNode.parentNode);
                placeholderDiv.remove();
                if(isExtra) currentPlayer[4]--; else currentPlayer[3]--;
                document.getElementsByClassName(turn_player_color + "_main_count")[0].innerText = currentPlayer[3];
                document.getElementsByClassName(turn_player_color + "_extra_count")[0].innerText = currentPlayer[4];
                document.getElementsByClassName(turn_player_color + "_count")[0].innerText = currentPlayer[3] + currentPlayer[4];

                RemoveFromOverallList(card);

            } else {

                cardNode.parentNode.classList.add("picking");
                await delay(100);
                cardNode.parentNode.classList.remove("picking");

                let placeholderDiv = document.createElement('div');
                placeholderDiv.classList.add(card);
                placeholderDiv.style.display = "none";
                insertAfter(cardNode.parentNode, placeholderDiv);
                var cardType = cardNode.parentNode.classList[1];
                var referenceInsertNode
                var cardNumber = parseInt(cardNode.parentNode.classList[2].substring(10, cardNode.parentNode.classList[2].length))
                var nextNeighborNodeNumber;
                for(var ii = 0; ii < cardNumber; ii++){
                    currentlyInspectedNode = document.getElementById(turn_player_color + "DeckModal").querySelector(".cardNumber"+ii);
                    if(currentlyInspectedNode != null){
                        if(ii < cardNumber){
                            nextNeighborNodeNumber = ii;
                        }
                    }
                }
                var nextNeighbourNode = document.getElementById(turn_player_color + "DeckModal").querySelector(".cardNumber"+nextNeighborNodeNumber);
                if(nextNeighborNodeNumber != null && nextNeighbourNode.classList.contains("extra") == isExtra){
                    referenceInsertNode = nextNeighbourNode;
                } else {
                    referenceInsertNode = document.getElementById(turn_player_color + "DeckModal").querySelector('.' + cardType);
                }
                insertAfter(referenceInsertNode, cardNode.parentNode);
                cardNode.parentNode.classList.add("selectedCard");
                if(isExtra) currentPlayer[4]++; else currentPlayer[3]++;
                document.getElementsByClassName(turn_player_color + "_main_count")[0].innerText = currentPlayer[3];
                document.getElementsByClassName(turn_player_color + "_extra_count")[0].innerText = currentPlayer[4];
                document.getElementsByClassName(turn_player_color + "_count")[0].innerText = currentPlayer[3] + currentPlayer[4];

                AddToOverallList(card, currentPlayer[2]);
            }
        }
    }

    async function AddToOverallList(card, cardColor) {

        var actuallCardNode = document.getElementById(card).parentNode;
        var cardNode = actuallCardNode.cloneNode(true);
        cardNode.childNodes[1].id = card + "_copy";
        cardNode.onclick = null;
        cardNode.classList.add("copy");
        var turn_player_color = "all";
        let ownerMarker = document.createElement('span');
        cardNode.insertBefore(ownerMarker, cardNode.childNodes[1]);
        ownerMarker.classList.add("owner-marker");
        ownerMarker.style.background = cardColor;

        var cardType = cardNode.classList[1];
        var referenceInsertNode
        var cardNumber = parseInt(cardNode.classList[2].substring(10, cardNode.classList[2].length))
        var nextNeighborNodeNumber;
        for(var ii = 0; ii < cardNumber; ii++){
            currentlyInspectedNode = document.getElementById(turn_player_color + "DeckModal").querySelector(".cardNumber"+ii);
            if(currentlyInspectedNode != null){
                if(ii < cardNumber){
                    nextNeighborNodeNumber = ii;
                }
            }
        }
        var nextNeighbourNode = document.getElementById(turn_player_color + "DeckModal").querySelector(".cardNumber"+nextNeighborNodeNumber);
        if(nextNeighborNodeNumber != null){
            referenceInsertNode = nextNeighbourNode;
        } else {
            referenceInsertNode = document.getElementById(turn_player_color + "DeckModal").querySelector('.' + cardType);
        }
        insertAfter(referenceInsertNode, cardNode);
    }

    async function RemoveFromOverallList(card) {
        var cardNode = document.getElementById(card + "_copy").parentNode;
        cardNode.remove();
    }

	function insertAfter(referenceNode, newNode) {
        isInModal = document.getElementById("modals").contains(newNode);
		referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
	}

    function activateButton(color) {

        // if(isTouchDevice()){
        //     currentColor = color;
        //     document.getElementsByClassName(color)[0].addEventListener("touchend", touchend);
        //     document.getElementsByClassName(color)[0].addEventListener("touchstart", touchstart(color));
        // }
        // Alle Buttons durchgen und ein-/ausschalten
        for (var i = 0; i < player.length; i++) {
            // Button laden
            var button = document.getElementById(player[i][0]);
            var playerCard = document.getElementById(player[i][0] +"PlayerCard")
            var playerCardLamp = document.getElementsByClassName("playerCardLamp" + player[i][0])[0];

            // gelickter Button ist aktueller Button
            if (player[i][0] == color) {
                // Button einschalten und Flag für Farbe setzen
                playerCard.style.outlineStyle = "solid";
                playerCard.style.outlineWidth = "0.2rem";
                playerCard.style.outlineColor = player[i][2];
                playerCardLamp.style.backgroundColor = player[i][2];
                playerCardLamp.style.filter = "drop-shadow(0rem 0rem 0.2rem " + player[i][2] + ")";
                button.classList.add('button_filter_' + player[i][0]);
                player[i][1] = 1;
            }
            else // Button ist anderer Button
            {
                // Button ausschalten und Falg entfernen
                playerCard.style.outlineStyle = "solid";
                playerCard.style.outlineWidth = "0rem";
                playerCard.style.outlineColor = player[i][2];
                playerCardLamp.style.backgroundColor = "transparent";
                playerCardLamp.style.filter = "none";
                button.classList.remove('button_filter_' + player[i][0]);
                player[i][1] = 0;
            }
        }
    }

	function openDeckModal(){
		for (var i = 0; i < player.length; i++) {
			var headder = document.getElementById(player[i][0] + "DeckList");
			var playerName = document.getElementById(player[i][0] + "PlayerName").value;
			headder.innerText = playerName + "'s Decklist"

            var modal = document.getElementById(player[i][0]+"DeckModal");
            if(modal.querySelector('.mainDeck').classList.contains("hiddenDeck")){
                modal.querySelector('.mainDeck').classList.remove("hiddenDeck");
                modal.querySelector('.extraDeck').classList.add("hiddenDeck");
                modal.querySelector('.changeDeckButtonTitle').innerText = "Wechlse zu Extra";
            }
        }
	}

    function openDeckModalDoubleClick(color){
        $('#'+color+"DeckModal").modal('show');
        openDeckModal();
	}

    // var onlongtouch;
    // var timer;
    // var currentColor;
    // function touchstart(color) {
    //     if (!timer) { 
    //         timer = setTimeout(onlongtouch(color), 1000); 
    //     }
    // }
    // function touchend() {
    //     if (timer) {
    //         clearTimeout(timer);
    //         timer = null;
    //     }
    // }
    // onlongtouch = function (color) {
    //     timer = null;
    //     openDeckModalDoubleClick(color);
    // };

	function showMenu(){
		document.getElementById("left-controls").classList.add("expandend");
	}
	function closeMenu(){
		document.getElementById("left-controls").classList.remove("expandend");
	}

	function scrollToMonster(){
		document.getElementById("MonsterHeadder").scrollIntoView();
	}
	function scrollToSpells(){
		document.getElementById("SpellsHeadder").scrollIntoView();
	}
	function scrollToTraps(){
		document.getElementById("TrapsHeadder").scrollIntoView();
	}
    function scrollToExtra(){
		document.getElementById("ExtraHeadder").scrollIntoView();
	}

    function changeDeck(color){
        var modal = document.getElementById(color+"DeckModal");
        if(modal.querySelector('.mainDeck').classList.contains("hiddenDeck")){
            modal.querySelector('.mainDeck').classList.remove("hiddenDeck");
            modal.querySelector('.extraDeck').classList.add("hiddenDeck");
            modal.querySelector('.changeDeckButtonTitle').innerText = "Wechlse zu Extra";
        }else{
            modal.querySelector('.extraDeck').classList.remove("hiddenDeck");
            modal.querySelector('.mainDeck').classList.add("hiddenDeck");
            modal.querySelector('.changeDeckButtonTitle').innerText = "Wechlse zu Main";
        }
        
    }

    function isTouchDevice() {
        return (('ontouchstart' in window) ||
           (navigator.maxTouchPoints > 0) ||
           (navigator.msMaxTouchPoints > 0));
      }